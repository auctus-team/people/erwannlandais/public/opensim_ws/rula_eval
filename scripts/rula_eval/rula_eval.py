#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
.. module:: opensim parser compatible with numpy
       :platform: ubuntu
       :synopsis: parse OpenSim motion files into numpy matrices
.. moduleauthor:: Galo Maldonado <galo_xav@hotmail.com>
"""

import os 
import xml.etree.ElementTree as xml
import numpy as np
import pinocchio as se3

import glob

import csv

import tf.transformations

def readMot(filename, verbose=False, isDeg = False):
    """This parser can be used with osim motion or force files 
    param filename: the complete filename. Accepted extensions are 
    ".mot" and "sto".
    type filename: str.
    param verbose: false by default.
    type: boolean.
    returns: time numpy array, data numpy matrix and column headers 
        list of data
    """
    data = []
    time = []
    file_extension = os.path.splitext(filename)[1][1:]
    
    # Verify extension is correct
    if file_extension not in ['mot', 'sto']:
        print('File extension is not recognized. Only OpenSim .mot and .sto files can be parsed')
        if (verbose): print( 'File extension given is .'+file_extension)
        return
    
    # Try to open the file
    try:
        f = open(filename, 'r')
    except IOError:
        print('cannot open', filename)
    
    # Read the file    
    with open(filename, 'r') as f:
        filename = f.readline().split()[0]
        if (verbose): print('Reading file: '+filename)

        # The header is not really informative
        while True:
            try:
                line = f.readline().split()[0]
            except IndexError:
                line = f.readline()
                    
            if line[0:9] == 'endheader':
                break        

        # Get the colheaders
        if (verbose): print("Reading the colheaders")
        col_headers = f.readline().split()[1:]

        # Read time and data from file
        for rows in f:
            data.append(rows.split()[1:])
            time.append(float(rows.split()[0]))
        
        for rows in range (0,len(data[:])):
            for cols in range (0,len(data[0])):
                if cols in (3,4,5):
                    # translations
                    data[rows][cols] = float(data[rows][cols])
                else:
                    if isDeg:
                    # store rotations in radians, defualt in opensim is degrees
                        data[rows][cols] = np.deg2rad(float(data[rows][cols]))
                    else:
                        data[rows][cols] = float(data[rows][cols])
        
        return np.array(time), np.matrix(data), col_headers
    

def printConfig(urdf_model,urdf_data):
    """!
    
    Displays the position and orientation of each of the joints of the .urdf
    model, expressed into the global referential associated with the .urdf. 
    
    @param urdf_model : Pinocchio model object.
        Pinocchio object containing all the physical description of the loaded URDF model, 
        including kinematic and inertial parameters defining its structure.
        Please check https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/master/doxygen-html/md_doc_a-features_intro.html 
        for more informations.
        
    @param urdf_data : Pinocchio data object.
         Pinocchio object containing all the values of the loaded URDF model which
         are the result of a computation. (joint configuration, velocity, ...).

    @return None.

    """    
    print("----- Translation -----")    
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        print(("{:<24} : {: .4f} {: .4f} {: .4f}"
              .format( name, *oMi.translation.T.flat )))
    print("----- Rotation -----")
    for name, oMi in zip(urdf_model.names, urdf_data.oMi):
        homo_RM = np.eye(4,4)
        homo_RM[:3,:3] = oMi.rotation[:3,:3]
        r,p,y = tf.transformations.euler_from_matrix(homo_RM)
        print(("{:<24} : {: .4f} {: .4f} {: .4f} "
            .format(name, *list([r,p,y]))) )


def all_close(goal, actual, tolerance,dim = 1, verbose = False):

    if type(goal) is list:
        for index in range(len(goal)):    
            if type(tolerance) is list:
                tol = tolerance[index]
            else:
                tol = tolerance       
            # print(tolerance)     
            # print(tol)
            #print(type(tolerance))
            if abs(actual[index] - goal[index]) > tol:
                
                if (verbose):
                    print("Error : difference detected at index %i : actual : %f ; goal : %f; tolerance : %f"%(index,actual[index],goal[index],tol))
                return False    
    else:
        Lg = []
        La = []
        for k in range(goal.shape[dim]):
            if dim ==1:
                Lg.append(goal[0,k])
                La.append(actual[0,k])
            else:
                Lg.append(goal[k,0])
                La.append(actual[k,0])

        return(all_close(Lg,La,tolerance,dim,verbose))

    return(True)      

class SpeedAcc():
    """!
    
    Returns speed and acceleration of each joint in a .csv.
    
    """
    def __init__(self, urdf_filename, mot_path,calib_frame_index=-1, byFolder = True):
        ## Pinocchio object containing all the physical description of the loaded URDF model.
        self.urdf_model    = se3.buildModelFromUrdf(urdf_filename)
        ## Pinocchio object containing all the values of the loaded URDF model which are the result of a computation.
        self.urdf_data = self.urdf_model.createData()

        ## True : check all .mot files at the same time
        ## False : check only 1 .mot file (equal to mot_path)
        self.byFolder = byFolder

        self.fullPoses = False

        
        if self.byFolder:
            if (type(mot_path) == type([])):
                L_mot_filename = mot_path
                L = mot_path[0].split("/")[:-1] 
                s = "/"
                for j in range(len(L)):
                    s +=  L[j] + "/"
                mot_path = s
                print(mot_path)
            else:   
                if (mot_path[-1] != "/"):
                    mot_path += "/"            
                filepath= mot_path+ "*.mot"
                L_mot_filename = glob.glob(filepath)            
        else:
            filepath = mot_path
            L_mot_filename = [filepath]


        L_D_speed = []
        L_D_acc = []

        jn = []

        for mot_filename in L_mot_filename:

            D_speed = {}
            D_acc = {}
            self.armSupported = False
            
            self.mot_file = mot_filename
            time,Q,joint_names = readMot(mot_filename)
            if jn == []:
                jn = joint_names
            val = 0

            for k in range(len(joint_names)):
                D_speed[joint_names[k]] = []
                D_acc[joint_names[k]] = []

            print("File %s read. Begin analysis"%(mot_filename))

            typeTrial = int(mot_filename.split("/")[-1].split("_")[1])

            ## compute articular speed
            DS_time = time[1:]

            for i in range(1, len(time)):
                dt = time[i] - time[i-1]
                for j in range( Q.shape[1]):
                    D_speed[joint_names[j]].append( (Q[i,j]-Q[i-1,j])/dt)
            print("File %s : Speed done."%(mot_filename))

            ## compute articular acc
            # print(len(D_speed[joint_names[0]]))
            # print(len(time))
            for i in range(1, len(DS_time) ):
                dt = DS_time[i] - DS_time[i-1]
                
                for j in range( Q.shape[1]):
                    DS = D_speed[joint_names[j]]

                    #print(DS)
                    D_acc[joint_names[j]].append( (DS[i]-DS[i-1])/dt )
            print("File %s : Acceleration done."%(mot_filename))

            L_D_speed.append(D_speed)
            L_D_acc.append(D_acc)

        if self.byFolder:
            if self.fullPoses:
                filename = mot_path+"SA_allPoses_result.csv"

            else:

                filename = mot_path+"SA_result.csv"
                
        else:
            filename = mot_path.split(".mot")[0]
            if self.fullPoses:
                filename+= "SA_allPoses_result.csv"
                
            else:
                filename+= "SA_result.csv"

        self.saveCSV(filename,L_D_speed, L_D_acc, L_mot_filename, jn)

                    
    def saveCSV(self,filename,L_D_speed,L_D_acc, L_mot_name, joint_names):
        """!
        
        Format : 

        First, resume : max / min / med of each joint, for speed and acc

        Then, all datas, by time

        """
        blank = ["" for _ in range(len(joint_names)+1)]

        with open(filename, 'w', encoding='UTF8') as f:

            writer = csv.writer(f)
            writer.writerow(joint_names)
            for i in range(len(L_mot_name)):
                mot_name = L_mot_name[i].split("/")[-1]
                mot_stop = blank.copy()
                mot_stop[0] = mot_name
                writer.writerow(mot_stop)
                ## for speed resume
                writer.writerow(["Speed Max/Min/Med (rad/s)"])

                L_all = []

                D_cons = L_D_speed[i]

                for k in range(len(joint_names)):
                    mx = np.max(D_cons[joint_names[k]])
                    mn = np.min(D_cons[joint_names[k]])
                    med = np.median(D_cons[joint_names[k]])
                    L_all.append([mx,mn,med])

                writer.writerow(L_all)

                #writer.writerow(blank)

                ## for acc resume
                writer.writerow(["Acc Max/Min/Med (rad/s)"])
                # writer.writerow(joint_names)
                L_all = []

                D_cons = L_D_acc[i]

                for k in range(len(joint_names)):
                    mx = np.max(D_cons[joint_names[k]])
                    mn = np.min(D_cons[joint_names[k]])
                    med = np.median(D_cons[joint_names[k]])
                    L_all.append([mx,mn,med])

                writer.writerow(L_all)

                writer.writerow(blank)


class ErgoEval():

    def __init__(self, urdf_filename, mot_path,calib_frame_index=-1, byFolder = True, desactivateLegsComputing= False, ergoEval = "RULA", fullPoses = False):
        """!
        
        Fullposes : instead of trying to find static poses, just do as other persons do : evaluate on full trial, whatever.
        
        """
        ## Pinocchio object containing all the physical description of the loaded URDF model.
        self.urdf_model    = se3.buildModelFromUrdf(urdf_filename)
        ## Pinocchio object containing all the values of the loaded URDF model which are the result of a computation.
        self.urdf_data = self.urdf_model.createData()

        ## True : check all .mot files at the same time
        ## False : check only 1 .mot file (equal to mot_path)
        self.byFolder = byFolder

        self.fullPoses = fullPoses

        self.desactivateLegsComputing = desactivateLegsComputing
        
        ergoEvalAvailable = ["RULA","REBA"]
        self.ergoEval = "RULA"
        if ergoEval in ergoEvalAvailable:
            self.ergoEval = ergoEval

        
        if self.byFolder:
            if (type(mot_path) == type([])):
                L_mot_filename = mot_path
                L = mot_path[0].split("/")[:-1] 
                s = "/"
                for j in range(len(L)):
                    s +=  L[j] + "/"
                mot_path = s
                print(mot_path)
            else:   
                if (mot_path[-1] != "/"):
                    mot_path += "/"            
                filepath= mot_path+ "*.mot"
                L_mot_filename = glob.glob(filepath)            
        else:
            filepath = mot_path
            L_mot_filename = [filepath]


        L_Ergo_arm = []
        L_Ergo_oth = []
        L_Ergo_arm_det = []
        L_Ergo_oth_det = []
        
        l = 0

        trialArms = [4,5,6]

        for mot_filename in L_mot_filename:

            self.armSupported = False
            
            self.mot_file = mot_filename
            time,Q,joint_names = readMot(mot_filename)
            val = 0

            print("File %s read. Begin analysis"%(mot_filename))

            typeTrial = int(mot_filename.split("/")[-1].split("_")[1])
            self.armSupported = (typeTrial in trialArms)

            if not self.desactivateLegsComputing:
                l_calib = joint_names.index("torso_ty")

                if (calib_frame_index == -1):
                    ## try to get calib frame index from filename
                    val = int(mot_filename.split("/")[-1].split("_")[-2])
                    self.y_calib = Q[val,l_calib]                
                
                elif type(calib_frame_index) == type(1):
                    val = calib_frame_index
                    self.y_calib = Q[val,l_calib]
                else:
                    if (l < len(calib_frame_index)):
                        val = calib_frame_index[l]
                        self.y_calib = Q[val,l_calib]                    
                        l+=1
                    else:
                        ## do not update y_calib
                        a = 1
            else:
                self.y_calib = -1

                

            self.D_name_qInd = {}
            self.D_name_ind = {}
            #print(joint_names)
            self.getJointProps(joint_names)

            self.lastQ = se3.neutral(self.urdf_model)

            self.trans_joints = ["torso_tx",
                            "torso_ty",
                            "torso_tz"
                            ]

            arm_joints = [
                "arm_flex_r",
                "arm_add_r",
                "arm_rot_r",
                "elbow_flex_r",
                "pro_sup_r",
                "wrist_flex_r",
                "wrist_dev_r",
                "hand_finger_flex_r",
                "arm_flex_l",
                "arm_add_l",
                "arm_rot_l",
                "elbow_flex_l",
                "pro_sup_l",
                "wrist_flex_l",
                "wrist_dev_l",
                "hand_finger_flex_l"
            ]
            Q_arms = np.array([])
            Q_other = np.array([])

            other_joints = []

            ## differenciate datas linked to arm / linked to other parts

            for k in range(len(joint_names)):
                if joint_names[k] in arm_joints:
                    if Q_arms.shape[0] == 0:
                        Q_arms = Q[:,k]
                    else:
                        Q_arms = np.c_[Q_arms,Q[:,k]]
                else:

                    other_joints.append(joint_names[k])
                    if Q_other.shape[0] == 0:
                        Q_other = Q[:,k]
                    else:
                        Q_other = np.c_[Q_other,Q[:,k]]         
            
            ## Pr moment : 
            ### Faire tri par groupe d'articulation 
            ### Faire un premier tri avec une grosse tolérance sur grosse durée : évaluation pose statique
            ### Dans second tri, évaluer avec petite tolérance sur petits instants : évaluation répétition pose
            ### (dans second tri, petits instants = pour enlever bruit)
            ### Si dans second tri on trouve une répétition suffisante, ajouter score pour toute la posture
            ### Evaluer Ergo par posture (= premier tri), partagé entre groupe 1 et groupe 2  

            ## NB : si veut annuler : 
            ## * Déf D_ind_pos, D_ind_time comme correspondant à toutes les données
            ## * Déf D_step comme score égal à 1 ou 0 sur ensemble.

            ## gros tri
            g_tol= [9*np.pi/180 for _ in range(Q_arms.shape[1])]
            g_ts = 3.0
            ## petit tri
            s_tol= [4*np.pi/180 for _ in range(Q_arms.shape[1])]
            s_ts = 0.5
            # print(tol)

            ## keys : [interval_i,interval_i+1, ...], where interval_i corresponds
            ## to a duration where we need to add +1

            all_tol = [g_tol,s_tol]
            all_ts = [g_ts,s_ts]

            if self.fullPoses :
                indPos = 0
                self.D_ind_time_arm = {}
                self.D_ind_pos_arm = {}
                self.D_ind_pos_arm[indPos] = Q_arms[0,:]
                self.D_ind_time_arm[indPos] = [[time[0],time[-1]]]
                self.D_arm_step = {}
                self.D_arm_step[indPos] = []
                self.staticAction_arm = False

                
            else:
                self.D_ind_pos_arm,self.D_ind_time_arm, self.D_arm_step, self.staticAction_arm = self.getPostureInfos(time,Q_arms,all_tol,all_ts)
            print("File %s : Fixed postures based on arms only found."%(mot_filename))



            ## gros tri
            g_tol= [9*np.pi/180 for _ in range(Q_other.shape[1])]
            g_ts = 3.0
            ## petit tri
            s_tol= [4*np.pi/180 for _ in range(Q_other.shape[1])]
            s_ts = 0.5

            for k in range(len(other_joints)):
                if other_joints[k] in self.trans_joints:
                    g_tol[k] = 0.05
                    s_tol[k] = 0.01
            
            if self.fullPoses :
                indPos = 0
                self.D_ind_time_oth = {}
                self.D_ind_pos_oth = {}
                self.D_ind_pos_oth[indPos] = Q_arms[0,:]
                self.D_ind_time_oth[indPos] = [[time[0],time[-1]]]
                self.D_oth_step = {}
                self.D_oth_step[indPos] = []
                self.staticAction_oth = False
            else:
                self.D_ind_pos_oth,self.D_ind_time_oth, self.D_oth_step, self.staticAction_oth = self.getPostureInfos(time,Q_other,all_tol,all_ts)
            print("File %s : Fixed postures based on other joints done."%(mot_filename))

            # print("Arm : ")
            # print(self.D_ind_time_arm)
            # print(self.D_arm_step)
            # print("Other : ")
            # print(self.D_ind_time_oth)
            # print(self.D_oth_step)      

            # exit()  

            ####  ==== to activate biais ===

            ## always muscleScore
            # for keys in self.D_arm_step:
            #     self.D_arm_step[keys] = [ [0.0, np.inf] ]
            
            # for keys in self.D_oth_step:
            #     self.D_oth_step[keys] = [ [0.0, np.inf] ]

            ## never muscleScore
            # for keys in self.D_arm_step:
            #     self.D_arm_step[keys] = []
            
            # for keys in self.D_oth_step:
            #     self.D_oth_step[keys] = []

            ## for now, check all postures for both arm and other
            ## saves worst Ergo score for each posture
            ## such as we have : M = [beg_time,end_time,worst_Ergo]

            Ergo_arm, Ergo_oth, Ergo_arm_det, Ergo_oth_det = self.evaluateErgo(time,Q,joint_names)

            print("File %s : Ergo computed."%(mot_filename))


            L_Ergo_arm.append(Ergo_arm)
            L_Ergo_oth.append(Ergo_oth)

            L_Ergo_arm_det.append(Ergo_arm_det)
            L_Ergo_oth_det.append(Ergo_oth_det)

            print("%s done. "%mot_filename )
            #exit()
        if self.byFolder:
            if self.fullPoses:
                filename = mot_path+"%s_allPoses_result.csv"%(self.ergoEval)

            else:

                filename = mot_path+"%s_result.csv"%(self.ergoEval)
                
        else:
            filename = mot_path.split(".mot")[0]
            if self.fullPoses:
                filename+= "_%s_allPoses_result.csv"%(self.ergoEval)
                
            else:
                filename+= "_%s_result.csv"%(self.ergoEval)
                
        self.saveCSV(filename,L_Ergo_arm,L_Ergo_oth,L_mot_filename, L_Ergo_arm_det,L_Ergo_oth_det)
        print(".mot result is saved at %s"%filename)


    def printQ(self,Q,L_name):
        print("-------")
        for i in range(Q.shape[1]):
            name = L_name[i]
            print("Name : %s ; value : %f"%(name,Q[0,i]))


    def saveCSV(self,filename,L_Ergo_arm,L_Ergo_oth,L_mot_name, L_Ergo_arm_det = [], L_Ergo_oth_det = []):
        """

        L_Ergo_arm : list of list.
                    L_Ergo_arm[i] : [Ergo_med,Ergo_max,Ergo_min] for posture i
        
        filename : should end with .csv

        https://www.pythontutorial.net/python-basics/python-write-csv-file/
        """

        details_act = False
        if self.ergoEval == "RULA":
            header = ["Posture_type", "beg_time", "end_time", "%s_time"%(self.ergoEval), "%s_score_total"%(self.ergoEval), 
                      "upper arm score", "lower arm score", "final wrist score", "wrist twist",
                       "muscleScore_A", "neck score", "trunk score", "leg score", "muscleScore_B", "RULA_score_A", "RULA_score_B"]

            if len(L_Ergo_arm_det[0]) > 0:
                header2 = ["Posture_type", "beg_time", "end_time", "RULA_time", "RULA_score_total",self.all_jn[1][0], self.all_jn[2][0], self.all_jn[3][0], "UPPER ARM SCORE",
                        self.all_jn[4][0], self.all_jn[5][0], "LOWER ARM SCORE", 
                        self.all_jn[6][0], self.all_jn[7][0], "FINAL WRIST SCORE",
                        "WRIST TWIST", "muscleScore_A", 
                        self.all_jn[10][0], self.all_jn[11][0], self.all_jn[12][0],"NECK SCORE",
                        self.all_jn[13][0], self.all_jn[14][0], self.all_jn[15][0], "TRUNK SCORE", 
                        "LEG SCORE", "muscleScore_B", "RULA_score_A", "RULA_score_B"]
                details_act = True


        elif self.ergoEval == "REBA":
            header = ["Posture_type", "beg_time", "end_time", "%s_time"%(self.ergoEval), "REBA_score_total", "leg score", 
                      "trunk_score", "neck_score", "shock force", "upper arm score", "lower arm score", "wrist score","REBA_score_A", "REBA_score_B","Activity_score"
                        ]

            if len(L_Ergo_arm_det[0]) > 0:
                header2 = ["Posture_type", "beg_time", "end_time", "REBA_time", "REBA_score_total","LEG SCORE",
                           self.all_jn[1][0], self.all_jn[2][0], self.all_jn[3][0], "TRUNK SCORE",
                        self.all_jn[4][0], self.all_jn[5][0], self.all_jn[6][0], "NECK SCORE", "SHOCK FORCE",
                        self.all_jn[8][0], self.all_jn[9][0],  self.all_jn[10][0], "UPPER ARM SCORE",
                        self.all_jn[11][0],"LOWER ARM SCORE",
                        self.all_jn[12][0], self.all_jn[13][0], self.all_jn[14][0], "WRIST SCORE", 
                       "REBA_SCORE_A", "REBA_SCORE_B","Activity_score_1","Activity_score_large_mvt"]
                details_act = True

        blank = ["" for _ in header]

        with open(filename, 'w', encoding='UTF8') as f:

            writer = csv.writer(f)
            writer.writerow(header)

            for k in range(len(L_mot_name)):

                mot_name = L_mot_name[k].split("/")[-1]

                mot_name += " | max_min_med"
                
                Ergo_arm = L_Ergo_arm[k] ## should be [med,max,min]
                Ergo_oth = L_Ergo_oth[k]

                mot_stop = blank.copy()
                mot_stop[0] = mot_name

                writer.writerow(blank)
                writer.writerow(mot_stop)

                line = blank.copy()
                line[0] = "Arm"
                for l in range(Ergo_arm[0].shape[0]):

                    line[1] = Ergo_arm[0][l,0]     
                    line[2] = Ergo_arm[0][l,1]                                             
                    for i in range(3,len(line)):
                        col = [0 for _ in range(len(Ergo_arm))]

                        for j in range(len(col)):

                            #print(Ergo_arm[j].shape)
                            
                            col[j] = Ergo_arm[j][l,i-1]
                        line[i] = col
                    writer.writerow(line)

                line[0] = "Body"
                for l in range(Ergo_oth[0].shape[0]):
                    line[1] = Ergo_oth[0][l,0]     
                    line[2] = Ergo_oth[0][l,1]                                             
                    for i in range(3,len(line)):
                        col = [0 for _ in range(len(Ergo_oth))]
                        for j in range(len(col)):
                            col[j] = Ergo_oth[j][l,i-1]
                        line[i] = col
                    writer.writerow(line)

                # for l in range(Ergo_arm.shape[0]):
                #     line[1:] = Ergo_arm[l,:]
                #     writer.writerow(line)
                # line[0] = "Body"
                # for l in range(Ergo_oth.shape[0]):
                #     line[1:] = Ergo_oth[l,:]
                #     writer.writerow(line)     

            if (details_act):
                blank = ["" for _ in header2]

                for l in range(5):
                    writer.writerow(blank)
                
                info = blank.copy()
                info[0] = "Details about computing"
                writer.writerow(info)     
                writer.writerow(header2)           
                for k in range(len(L_mot_name)):
                    mot_name = L_mot_name[k].split("/")[-1]
                    mot_name += " | max_min_med"

                    Ergo_arm = L_Ergo_arm_det[k]
                    Ergo_oth = L_Ergo_oth_det[k]

                    mot_stop = blank.copy()
                    mot_stop[0] = mot_name

                    writer.writerow(blank)
                    writer.writerow(mot_stop)

                    line = blank.copy()
                    line[0] = "Arm"
                    for l in range(Ergo_arm[0].shape[0]):
                        #print("- l :", l)                        
                        line[1] = Ergo_arm[0][l,0]     
                        line[2] = Ergo_arm[0][l,1]                                             
                        for i in range(3,len(line)):

                            #print("-- i : ",  i)
                            col = [0 for _ in range(len(Ergo_arm))]
                            for j in range(len(col)):
                                #print("--- j : ",j)    
                                #print(Ergo_arm[j].shape)                            
                                col[j] = Ergo_arm[j][l,i-1]
                            line[i] = col
                        writer.writerow(line)

                    line[0] = "Body"
                    for l in range(Ergo_oth[0].shape[0]):
                        line[1] = Ergo_oth[0][l,0]     
                        line[2] = Ergo_oth[0][l,1]                               
                        for i in range(3,len(line)):
                            col = [0 for _ in range(len(Ergo_oth))]
                            for j in range(len(col)):
                                col[j] = Ergo_oth[j][l,i-1]
                            line[i] = col
                        writer.writerow(line) 

            


    def getPostureInfos(self,time,Q,all_tol,all_ts):
        """!
        
        Will returns : 

        D_ind_pos : articulation value of the postures
        D_ind_time : time value of the postures
        D_pos_step : which postures needs to add +1 (because step [7/13] is confirmed)
        staticAction : whether action can be considered as static (e.g > 50% of static posture on whole test) or not.

        
        """
        ## gros tri
        g_tol= all_tol[0]
        g_ts = all_ts[0]
        ## petit tri
        s_tol= all_tol[1]
        s_ts = all_ts[1]

        D_pos_step = {}
    
        D_ind_pos,D_ind_time = self.findSamePostureIntervals(time,Q,g_ts,g_tol,True)
        #print(list(D_ind_pos.keys()) )

        wholeDuration = time[-1] - time[0]

        staticDuration = 0

        for keys in D_ind_time:
            posture_times = D_ind_time[keys]
            beg_time = posture_times[0][0]
            end_time = posture_times[-1][1]

            staticDuration+= end_time-beg_time

            beg_index = np.where(time == beg_time)[0][0]
            end_index = np.where(time == end_time)[0][0]

            q_post = Q[beg_index:end_index,:]
            time_post = time[beg_index:end_index]

            D_ind_pos_posture,D_ind_time_posture = self.findSamePostureIntervals(time_post,q_post,s_ts,s_tol,True)
            L_add_7 = self.findMuscleScoreTime(D_ind_time_posture)

            D_pos_step[keys] = L_add_7

        staticAction = (staticDuration/wholeDuration > 1/2)

        return(D_ind_pos,D_ind_time,D_pos_step,staticAction)

    def evaluateRULAPosture(self,time,data,y_calib,jn,ind_pos,typ):
        """!
        
        allSCR (= allScore) : returns all the necessary scores to get RULA, in the same order as the procedure
        given by the article.

        allSCRDet (= allScoreDetailled) : returns all the articular values to get the different
        scores to get RULA, and the necessary scores to get RULA, in the same order as the procedure
        given by the article.
        
        
        """
        RULA = []

        allSCR = []
        allSCRDet = []

        L_spec = ["table_A","table_B","table_C","hand_orientation"]
        
        for k in range(data.shape[0]):

            Q = data[k,:]

            # print(self.neck_angle)

            if self.neck_angle != 99999:
                # print(Q[0,10])
                Q[0,10] = Q[0,10] - self.neck_angle
                # print(Q[0,10])
                # exit()

            ts = time[k]

            self.setQ(Q,jn)

            #print(Q[:,6:])

            L_scr_details = []

            L_inter_scr = []
            ## step 4 : upper arm score
            ## step 6 : lower arm score 
            ## step 8 : final wrist score            
            det_step = [4,6,8]

            ## posture_A_score
            curScore = 0
            for keys in range(1,9):
                if (keys in det_step):
                    L_inter_scr.append(curScore)
                    L_scr_details.append(curScore)
                    curScore = 0


                L_scr_step = []
                if keys == 5:

                    q_tst = np.zeros((1,11))

                    ## special hand orientation case
                    L_arms = ["pro_sup_r","pro_sup_l"]
                    L_ends = ["elbow_flex_r","elbow_flex_l"]
                    M_w_torso = self.getJointPose("torso_rotation")
                    M_torso_w = np.linalg.inv(M_w_torso)
                    # print(M_torso_w)

                    xy_normal = np.zeros((4,))
                    xy_normal[2] = 1.0
                    xy_normal[3] = 1.0

                    pt = np.zeros((3,))  

                    R_proj_xy = tf.transformations.projection_matrix(pt,xy_normal, pseudo = False)

                    for k in range(len(L_arms)):
                        ## reexpress in torso ref
                        M_lower_arm = self.getJointPose(L_arms[k])
                        # print(M_lower_arm)
                        M_arm_torso = np.dot(M_torso_w ,M_lower_arm)
                        # print(M_arm_torso)

                        M_end = self.getJointPose(L_ends[k])
                        M_end_torso = np.dot(M_torso_w ,M_end)

                        v_la = M_arm_torso[:,2].reshape((4,1)) #(M_arm_torso[:,3] - M_end_torso[:,3]).reshape((4,1))
                        v_la[3,0] = 1.0

                        
                        ## reproject on xy plan
                        vla_on_xy = np.dot(R_proj_xy,v_la)

                        ## get angle with y axis
                        # y = np.array([0.0,1.0,0.0])
                        # tht = np.arccos( np.dot(y,vla_on_xy[:3])/ np.linalg.norm(vla_on_xy[:3])   )

                        # print(tht)


                        tht = np.arctan(vla_on_xy[0,0]/vla_on_xy[1,0])

                        scr = 0
                        if (k == 0):
                            if tht >0:
                                scr = 1
                        else:
                            if tht < 0:
                                scr = 1
                        if (scr!= -1):
                            L_scr_step.append(scr)

                else:
                    elmts = self.all_ids[keys]

                    interval = self.all_intervals[keys]
                    score  = self.all_score[keys]

                    for l in elmts:

                        i = 0
                        scr = -2
                        while (i < len(interval) and scr == -2):
                            if Q[0,l] <= interval[i][1] and Q[0,l] >=interval[i][0]:
                                scr = score[i]

                            i+=1
                        if (scr!= -2):
                            L_scr_step.append(scr)
                try:
                    max_scr = np.max(L_scr_step)
                #max_scr = np.min(L_scr_step)
                except Exception as e:
                    print(e)
                    print(L_scr_step)
                    print(scr)
                    print(keys)
                    print(interval)
                    print(score)
                    print("elmts: ", elmts)  
                    for i in range(len(interval)):
                        print("interval : ", i, " : ", interval[i])
                        for l in elmts:
                            print(l, " : " ,Q[0,l])
                            print(Q[0,l] <= interval[i][1] )
                            print( Q[0,l] >=interval[i][0])
                    print(Q)
                    print(ts)
                    exit()


                L_scr_details.append(max_scr)
                #print("Step : %i (%s); score : %i"%(keys,self.all_jn[keys][0],max_scr))
                curScore+=max_scr
            
            ## last step : wrist twist
            L_inter_scr.append(curScore)
            #L_scr_details.append(curScore)

            ## evaluate table A            
            curScore = self.table_A[(L_inter_scr[0]-1)*3+(L_inter_scr[1]-1),(L_inter_scr[2]-1)*2+(L_inter_scr[3]-1) ]
            ## add MuscleScore_A

            val = 0
            L_tst = []
            if (typ == "Arm"):
                L_tst = [ind_pos]
            else:
                L_tst = list(self.D_arm_step.keys())

            l = 0

            while l < len(L_tst) and val == 0:
                ind = L_tst[l]
                all_times = self.D_arm_step[ind]
                for i in range(len(all_times)):
                    if (ts <= all_times[i][1] and ts >= all_times[i][0]):
                        curScore+=1
                        val = 1
                l+=1

            L_inter_scr.append(val)
            L_scr_details.append(val)        
            # print(curScore)
            # print(L_inter_scr)

            curScore_A = min(curScore,8)


            ## evaluate until table B

            ## step 13 : neck score
            ## step 16 : trunk score          
            det_step = [13,16]

            curScore = 0
            for keys in range(10,17):
                if (keys in det_step):
                    L_inter_scr.append(curScore)
                    L_scr_details.append(curScore)                    
                    curScore = 0

                L_scr_step = []
                
                if keys == 16:

                    ## manage particular step (legs)
                    M_torso = self.getJointPose("torso_ty")
                    # print(M_torso)
                    # print(y_calib)
                    # printConfig(self.urdf_model,self.urdf_data)
                    scr = 0
                    if not self.desactivateLegsComputing:
                        if abs(y_calib - M_torso[2,3]) > 0.08:
                            scr=2
                        else:
                            scr=1
                    else:
                        ## special case where we desactivate y_calib
                        ## (ex : if in tests, person is whether balanced
                        ## or only sitting)
                        scr = 1
                    L_scr_step.append(scr)

                else:
                    elmts = self.all_ids[keys]

                    interval = self.all_intervals[keys]
                    score  = self.all_score[keys]

                    for l in elmts:

                        i = 0
                        scr = -2
                        while (i < len(interval) and scr == -2):
                            if Q[0,l] <= interval[i][1] and Q[0,l] >=interval[i][0]:
                                scr = score[i]

                            i+=1

                        if (scr != -2):
                            L_scr_step.append(scr)
                
                max_scr = np.max(L_scr_step)
                #max_scr = np.min(L_scr_step)
                #print("Step : %i (%s); score : %i"%(keys,self.all_jn[keys][0],max_scr))
                L_scr_details.append(max_scr)
                curScore+=max_scr
                
            ## last step : leg score
            L_inter_scr.append(curScore)     
            #L_scr_details.append(curScore)                   

            ## evaluate table B            
            curScore = self.table_B[(L_inter_scr[-3]-1),(L_inter_scr[-2]-1)*2+(L_inter_scr[-1]-1) ]
            ## add MuscleScore_B

            L_tst = []
            if (typ == "Oth"):
                L_tst = [ind_pos]
            else:
                L_tst = list(self.D_oth_step.keys())

            l = 0
            while l < len(L_tst) and val == 0:
                ind = L_tst[l]
                all_times = self.D_oth_step[ind]
                for i in range(len(all_times)):
                    if (ts <= all_times[i][1] and ts >= all_times[i][0]) and val==0:
                        curScore+=1
                        val = 1
                l+=1

            L_inter_scr.append(val)
            L_scr_details.append(val)            
            # print(curScore)
            # print(L_inter_scr)
            # self.printQ(Q,jn)
            # print("time : ", ts)
            # exit()
            curScore_B = min(curScore,7)

            ## evaluate table_C
            curScore = self.table_C[(curScore_A-1),(curScore_B-1)]


            L_inter_scr.append(curScore_A)
            L_inter_scr.append(curScore_B)

            L_scr_details.append(curScore_A)            
            L_scr_details.append(curScore_B)

            RULA.append(curScore)

            allSCR.append(L_inter_scr)
            allSCRDet.append(L_scr_details)
            


        return( RULA, allSCR, allSCRDet )


    def evaluateREBAPosture(self,time,data,y_calib,jn,ind_pos,typ, staticAction):
        """!
        
        allSCR (= allScore) : returns all the necessary scores to get REBA, in the same order as the procedure
        given by the article.

        allSCRDet (= allScoreDetailled) : returns all the articular values to get the different
        scores to get REBA, and the necessary scores to get REBA, in the same order as the procedure
        given by the article.
        
        
        """
        REBA = []

        allSCR = []
        allSCRDet = []

        L_spec = ["table_A","table_B","table_C","hand_orientation"]
        
        for k in range(data.shape[0]):

            Q = data[k,:]

            # print(self.neck_angle)

            if self.neck_angle != 99999:
                # print(Q[0,10])
                Q[0,10] = Q[0,10] - self.neck_angle
                # print(Q[0,10])
                # exit()

            ts = time[k]

            self.setQ(Q,jn)

            #print(Q[:,6:])

            L_scr_details = []

            L_inter_scr = []
            ## step 4 : trunk score
            ## step 7 : neck score        
            det_step = [1,4]

            ## posture_A_score
            ## always start with 1 for legs
            curScore = 1
            ## for cases where penalty can only be added once
            L_checkForAdd = [2,3,5,6]
            alreadyAdded = False
            for keys in range(1,7):
                if (keys in det_step):

                    L_inter_scr.append(curScore)
                    L_scr_details.append(curScore)
                    curScore = 0

                if keys not in L_checkForAdd:
                    alreadyAdded = False

                L_scr_step = []

                elmts = self.all_ids[keys]

                interval = self.all_intervals[keys]
                score  = self.all_score[keys]

                for l in elmts:

                    i = 0
                    scr = -2
                    while (i < len(interval) and scr == -2):
                        if Q[0,l] <= interval[i][1] and Q[0,l] >=interval[i][0]:
                            scr = score[i]

                        i+=1
                    if (scr!= -2):
                        L_scr_step.append(scr)
                try:
                    max_scr = np.max(L_scr_step)
                    if keys in L_checkForAdd:
                        if alreadyAdded:
                            ## no double penalty
                            max_scr = 0
                        elif max_scr != 0:
                            ## indicates that penalty is already set
                            alreadyAdded = True

                #max_scr = np.min(L_scr_step)
                except Exception as e:
                    print(e)
                    print(L_scr_step)
                    print(scr)
                    print(keys)
                    print(interval)
                    print(score)
                    print("elmts: ", elmts)  
                    for i in range(len(interval)):
                        print("interval : ", i, " : ", interval[i])
                        for l in elmts:
                            print(l, " : " ,Q[0,l])
                            print(Q[0,l] <= interval[i][1] )
                            print( Q[0,l] >=interval[i][0])
                    print(Q)
                    print(ts)
                    exit()


                L_scr_details.append(max_scr)
                #print("Step : %i (%s); score : %i"%(keys,self.all_jn[keys][0],max_scr))
                curScore+=max_scr
            
            ## last step : neck score
            L_inter_scr.append(curScore)

            L_scr_details.append(curScore)
            ## evaluate table A            
            curScore = self.table_A[(L_inter_scr[1]-1),  (L_inter_scr[2]-1)*4+(L_inter_scr[0]-1) ]
            
            ## add shock force

            val = 0
            L_tst = []
            if (typ == "Arm"):
                L_tst = [ind_pos]
            else:
                L_tst = list(self.D_arm_step.keys())

            l = 0

            while l < len(L_tst) and val == 0:
                ind = L_tst[l]
                all_times = self.D_arm_step[ind]
                for i in range(len(all_times)):
                    if (ts <= all_times[i][1] and ts >= all_times[i][0]):
                        curScore+=1
                        val = 1
                l+=1

            L_inter_scr.append(val)
            L_scr_details.append(val)        
            # print(curScore)
            # print(L_inter_scr)

            curScore_A = min(curScore,9)

            ## evaluate until table B

            ## step 11 : upper arm
            ## step 12 : lower arm
            ## step 15 : wrist       
            det_step = [11,12]

            ## for cases where penalty can only be added once
            L_checkForAdd = [9,10,13,14]
            alreadyAdded = False

            curScore = 0
            for keys in range(8,15):
                if (keys in det_step):
                    if keys == 11 and self.armSupported:
                        ## leaning arms, added
                        curScore-=1                    
                    L_inter_scr.append(curScore)
                    L_scr_details.append(curScore)                    
                    curScore = 0

                if keys not in L_checkForAdd:
                    alreadyAdded = False

                L_scr_step = []
            
                elmts = self.all_ids[keys]

                interval = self.all_intervals[keys]
                score  = self.all_score[keys]

                for l in elmts:

                    i = 0
                    scr = -2
                    while (i < len(interval) and scr == -2):
                        if Q[0,l] <= interval[i][1] and Q[0,l] >=interval[i][0]:
                            scr = score[i]

                        i+=1

                    if (scr != -2):
                        L_scr_step.append(scr)
                
                max_scr = np.max(L_scr_step)
                if keys in L_checkForAdd:
                    if alreadyAdded:
                        ## no double penalty
                        max_scr = 0
                    elif max_scr != 0:
                        ## indicates that penalty is already set
                        alreadyAdded = True                
                #max_scr = np.min(L_scr_step)
                #print("Step : %i (%s); score : %i"%(keys,self.all_jn[keys][0],max_scr))
                L_scr_details.append(max_scr)
                curScore+=max_scr
                
            ## last step : wrist score
            L_inter_scr.append(curScore)     

                        #L_inter_scr.append(curScore)     
            L_scr_details.append(curScore)                   

            ## evaluate table B            

            curScore = self.table_B[(L_inter_scr[-3]-1),(L_inter_scr[-2]-1)*3+(L_inter_scr[-1]-1) ]
            ## no coupling

            curScore_B = min(curScore,9)

            ## evaluate table_C
            curScore = self.table_C[(curScore_A-1),(curScore_B-1)]

            # print(curScore_A)
            # print(curScore_B)

            # print(self.table_C[curScore_A-1,:])
            # exit()

            ## evaluate ActivityScore


            ## static parts + small range actions
            d2r = np.pi/180
            i = 0
            val = 0
            L_tst = []
            if (typ == "Arm"):
                L_tst = [ind_pos]
            else:
                L_tst = list(self.D_arm_step.keys())

            l = 0

            while l < len(L_tst) and val < 2:
                ind = L_tst[l]
                all_times = self.D_arm_step[ind]
                for i in range(len(all_times)):
                    if (ts <= all_times[i][1] and ts >= all_times[i][0]):
                        val = min(val+1,2)
                l+=1

            val1 = val
            valLC = 0
            ## large changes = not staticAction
            if not staticAction:
                valLC+=1
            
            val += valLC
            curScore+=val


            L_inter_scr.append(curScore_A)
            L_inter_scr.append(curScore_B)
            L_inter_scr.append(val)

            L_scr_details.append(curScore_A)            
            L_scr_details.append(curScore_B)
            L_scr_details.append(val1)
            L_scr_details.append(valLC)

            REBA.append(curScore)

            allSCR.append(L_inter_scr)
            allSCRDet.append(L_scr_details)
            


        return( REBA, allSCR, allSCRDet )


    def evaluateErgo(self,time,Q,joint_names):
        """!

        Evaluates Ergo for a given pose
        Need to get first whether step 7 and step 13
        are validated or not
        
        Returns : L_Ergo_score, organized as follows (for each pose, or each row; and for each metric evaluated [median,min,max]) :

        [beg_time, end_time, time_where_Ergo_is_given, Ergo_score_total, all_scores_part_A, all_scores_part_B, Ergo_score_A, Ergo_score_B]

        all_scores_part_A : upper arm score, lower arm score, final wrist score, wrist twist, muscleScore_A
        all_scores_part_B : neck score, trunk score, leg score, muscleScore_B

        L_Ergo_score = [ median_Ergo, max_Ergo, min_Ergo]
                
        """
        Ergo_mat = np.array([])

        L_Ergo_arm = [Ergo_mat.copy() for _ in range(3)]
        L_Ergo_arm_det = [Ergo_mat.copy() for _ in range(3)]

        L_Ergo_oth = [Ergo_mat.copy() for _ in range(3)]
        L_Ergo_oth_det = [Ergo_mat.copy() for _ in range(3)]


        if self.ergoEval == "RULA":
            thres_val = 20*np.pi/180

            self.all_jn = {
                1: ["arm_flex_r","arm_flex_l"],
                2: ["arm_add_r","arm_add_l"],
                3: ["torso_tilt", "torso_list", "torso_rotation"],
                4: ["elbow_flex_r","elbow_flex_l"],     
                5: ["hand_orientation"],
                6: ["wrist_flex_r","wrist_flex_l"],      
                7: ["wrist_dev_r","wrist_dev_l"],
                8: ["pro_sup_r","pro_sup_l"],            
                9: ["table_A"],
                10: ["neck_flexion"],
                11: ["neck_lateral_bending"],
                12: ["neck_rotation"],
                13: ["torso_tilt"],
                14: ["torso_list"],
                15: ["torso_rotation"],
                16: ["torso_ty"],
                17: ["table_B"],
                18: ["table_C"]
            }



            ## NB : table_A : add self.L_add_7
            ## table_B : add self.L_add_13
            ## by thresVal
            # self.all_intervals = {
            #     1: [ [-0.349,0.349], [-np.inf,-0.349], [0.349,0.785],  [0.785,1.57078], [1.57078,np.inf] ],
            #     2: [ [-np.inf,-thres_val], [-thres_val,np.inf]  ],
            #     3: [ [-np.inf,-thres_val], [-thres_val,thres_val],[thres_val, np.inf] ],
            #     4: [ [-np.inf,1.05], [1.05,1.745],[1.745,np.inf]  ], 
            #     6: [ [-np.inf,-0.262],[-0.262,-thres_val],[-thres_val,thres_val],[thres_val,0.262],[0.262, np.inf] ],           
            #     7: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],           
            #     8: [ [-np.inf,-2.9], [-2.9,2.9], [2.9,np.inf] ],      
            
            #     10: [ [-np.inf,-0.349], [-0.349,-0.174], [-0.174,thres_val],[thres_val,+np.inf]  ], ## thresVal
            #     #10: [ [-np.inf,-50*np.pi/180], [-50*np.pi/180,-10*np.pi/180], [-10*np.pi/180,30*np.pi/180],[30*np.pi/180,+np.inf]  ],            ## far higher reset, requiring new bounds
            #     11: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
            #     12: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
            #     13: [ [-np.inf,-1.047],[-1.047,-0.349],[-0.349,thres_val], [thres_val,np.inf] ],    
            #     14: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
            #     15: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ]
            # }
            ## withoutAnyThres (except for expressions)
            self.all_intervals = {
                1: [ [-0.349,0.349], [-np.inf,-0.349], [0.349,0.785],  [0.785,1.57078], [1.57078,np.inf] ],
                2: [ [-np.inf,-thres_val], [-thres_val,np.inf]  ],
                3: [ [-np.inf,-thres_val], [-thres_val,thres_val],[thres_val, np.inf] ],
                4: [ [-np.inf,1.05], [1.05,1.745],[1.745,np.inf]  ], 
                6: [ [-np.inf,-0.262],[-0.262,-1e-6],[-1e-6,1e-6],[1e-6,0.262],[0.262, np.inf] ],           
                7: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],           
                8: [ [-np.inf,-2.9], [-2.9,2.9], [2.9,np.inf] ],      
                10: [ [-np.inf,-0.349], [-0.349,-0.174], [-0.174,0.0],[0.0,+np.inf]  ],  ## origin 

                11: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
                12: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
                13: [ [-np.inf,-1.047],[-1.047,-0.349],[-0.349,0.0], [0.0,np.inf] ],    
                14: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
                15: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ]
            }        


            self.all_score = {
                1: [1,2,2,3,4],
                2: [1,0],
                3: [-1,0,-1],
                4: [ 2,1,2  ],
        
                6: [ 3,2,1,2,3],           
                7: [ 1,0,1 ],           
                8: [ 2,1,2 ],                     
                10: [ 3,2,1,4 ],
                11: [ 1,0,1 ],    
                12: [ 1,0,1 ],    
                13: [ 3,2,1,4 ],    
                14: [ 1,0,1 ],    
                15: [ 1,0,1]        
            }


            self.table_A = np.array([
                [1,2,2,2,2,3,3,3],
                [2,2,2,2,3,3,3,3],
                [2,3,3,3,3,3,4,4],
                [2,3,3,3,3,4,4,4],                                    
                [3,3,3,3,3,4,4,4],
                [3,4,4,4,4,4,5,5],
                [3,3,4,4,4,4,5,5],
                [3,4,4,4,4,4,5,5],
                [4,4,4,4,4,5,5,5],
                [4,4,4,4,4,5,5,5],
                [4,4,4,4,4,5,5,5],
                [4,4,4,5,5,5,6,6],
                [5,5,5,5,5,6,6,7],
                [5,6,6,6,6,7,7,7],
                [6,6,6,7,7,7,7,8],
                [7,7,7,7,7,8,8,9],            
                [8,8,8,8,8,9,9,9],   
                [9,9,9,9,9,9,9,9]
            ])

            self.table_B = np.array([
                [1,3,2,3,3,4,5,5,6,6,7,7],
                [2,3,2,3,4,5,5,5,6,7,7,7],
                [3,3,3,4,4,5,5,6,6,7,7,7],   
                [5,5,5,6,6,7,7,7,7,7,8,8],
                [7,7,7,7,7,8,8,8,8,8,8,8],
                [8,8,8,8,8,8,8,9,9,9,9,9]
            ])


            self.table_C = np.array([
                [1,2,3,3,4,5,5],
                [2,2,3,4,4,5,5],
                [3,3,3,4,4,5,6],
                [3,3,3,4,5,6,6],
                [4,4,4,5,6,7,7],   
                [4,4,5,6,6,7,7],
                [5,5,6,6,7,7,7],
                [5,5,6,7,7,7,7]                                                                   
            ])
        
        elif self.ergoEval =="REBA":
            d2r = np.pi/180            
            thres_val = 20*d2r

            zeroVal = 5*d2r


            ## legs always at 0 + both feet on ground ==> +1
            ## load force ==> always 0
            ## shock force (A) ==> muscleScore
            ## coupling (B) ==> always 0
            ## Activity score ==> muscleScore
            self.all_jn = {
                1: ["torso_tilt"],
                2: ["torso_list"],
                3: ["torso_rotation"],

                4: ["neck_flexion"],
                5: ["neck_lateral_bending"],
                6: ["neck_rotation"],

                7: ["table_A"],

                8: ["arm_flex_r","arm_flex_l"],
                9: ["arm_add_r","arm_add_l"],
                10: ["arm_rot_r","arm_rot_l"],

                11: ["elbow_flex_r","elbow_flex_l"],  

                12: ["wrist_flex_r","wrist_flex_l"],      
                13: ["wrist_dev_r","wrist_dev_l"],
                14: ["pro_sup_r","pro_sup_l"],    

                15: ["table_B"],
                16: ["table_C"]


            }


            ## withoutAnyThres (except for expressions)
            self.all_intervals = {

                1: [ [-np.inf,-60*d2r],[-60*d2r,-20*d2r],[-20*d2r,-zeroVal],[-zeroVal,zeroVal], [zeroVal,np.inf] ],    
                2: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
                3: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],

                4: [ [-np.inf,-20*d2r], [-20*d2r,-10*d2r], [-10*d2r,+np.inf]  ],  ## origin 
                5: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
                6: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],

                8: [ [-np.inf,-20*d2r], [-20*d2r,20*d2r], [20*d2r,45*d2r], [45*d2r,90*d2r], [90*d2r,np.inf]  ],  
                9: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],    
                10: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],
                
                11: [ [-np.inf,60*d2r], [60*d2r,100*d2r],[100*d2r,np.inf]  ], 

                12: [ [-np.inf,-15*d2r], [-15*d2r,15*d2r],[15*d2r,np.inf]  ], 
                13: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],  
                14: [ [-np.inf,-thres_val],[-thres_val,thres_val],[thres_val, np.inf] ],                  

            }        


            self.all_score = {

                1: [ 4,3,2,1,2],
                2: [ 1,0,1 ],    
                3: [ 1,0,1 ], 

                4: [ 2,1,2 ],    
                5: [ 1,0,1 ],    
                6: [ 1,0,1],

                8: [ 2,1,2,3,4],           
                9: [ 1,0,1 ],           
                10: [ 1,0,1 ],    

                11: [ 2,1,2 ],  

                12: [ 2,1,2],  
                13: [ 1,0,1 ],  
                14: [ 1,0,1 ]                             
      
            }


            self.table_A = np.array([
                [1,2,3,4,1,2,3,4,3,3,5,6],
                [2,3,4,5,3,4,5,6,4,5,6,7],
                [2,4,5,6,4,5,6,7,5,6,7,8],
                [3,5,6,7,5,6,7,8,6,7,8,9],                                    
                [4,6,7,8,6,7,8,9,7,8,9,9]
            ])

            self.table_B = np.array([
                [1,2,2,1,2,3],
                [1,2,3,2,3,4],
                [3,4,5,4,5,5],   
                [4,5,5,5,6,7],
                [6,7,8,7,8,8],
                [7,8,8,8,9,9]
            ])


            self.table_C = np.array([
                [1,1,1,2,3,3,4,5,6,7,7,7],
                [1,2,2,3,4,4,5,6,6,7,7,8],
                [2,3,3,3,4,5,6,7,7,8,8,8],
                [3,4,4,4,5,6,7,8,8,9,9,9],
                [4,4,4,5,6,7,8,8,9,9,9,9],   
                [6,6,6,7,8,8,9,9,10,10,10,10],
                [7,7,7,8,9,9,9,10,10,11,11,11],
                [8,8,8,9,10,10,10,10,10,11,11,11],
                [9,9,9,10,10,10,11,11,11,12,12,12],     
                [10,10,10,11,11,11,11,12,12,12,12,12],       
                [11,11,11,11,12,12,12,12,12,12,12,12],      
                [12,12,12,12,12,12,12,12,12,12,12,12],                                                                                                                                        
            ])

        self.all_ids = {}

        for keys in self.all_jn:
            jn = self.all_jn[keys]
            ids = []
            for k in range(len(jn)):
                if jn[k] in joint_names:
                    #ids.append(joint_names.index(jn[k]))
                    ids.append(self.D_name_ind[jn[k]])
                else:
                    ids.append(jn[k])
            self.all_ids[keys] = ids
        

        y_calib =self.y_calib

        self.neck_angle = 99999

        oriArmSup = self.armSupported

        self.firstPost = True
        self.armSupported = True


        ## first, with Ergo_arm
        for keys in self.D_ind_time_arm:
            ## select Q
            posture_times = self.D_ind_time_arm[keys]
            beg_time = posture_times[0][0]
            end_time = posture_times[-1][1]

            beg_index = np.where(time == beg_time)[0][0]
            end_index = np.where(time == end_time)[0][0]

            q_post = Q[beg_index:end_index,:]
            time_post = time[beg_index:end_index]     


            if self.ergoEval == "RULA":

                Ergo, allSCR, allSCRDet = self.evaluateRULAPosture(time_post,q_post,y_calib,joint_names,keys,"Arm")   


            elif self.ergoEval == "REBA":

                Ergo, allSCR, allSCRDet = self.evaluateREBAPosture(time_post,q_post,y_calib,joint_names,keys,"Arm", self.staticAction_arm)   


            if self.firstPost:
                if self.ergoEval == "RULA":
                ## take median neck angle for 1st posture (calib)

                    #exit()
                    q_neck = q_post[:,10]

                    q_neck = q_neck[:,0]

                    # print(np.median(q_neck,axis = 0)[0,0])

                    self.neck_angle = np.median(q_neck,axis = 0)[0,0]
                    # print(self.neck_angle)
                    # exit()
                self.firstPost = False
                self.armSupported = oriArmSup

                
            #if (k%1000 == 0):
            print("File %s : treatement : %s ; percentage of treatment : %3.2f "%(self.mot_file, 'Arm', beg_index*100.0/Q.shape[0]) )


            MmI = []
            for l in range(3):

                value = -1

                if l == 0:

                    value = np.max(Ergo)
                
                elif l == 1:
                    value = np.min(Ergo)                    
                
                elif l == 2:
                    value = np.median(Ergo)


                if (l < 2):
                    #allIndex = np.where(np.array(Ergo) == value)
                    # print(allIndex)
                    # print(value)
                    # exit()
                    index = np.where(np.array(Ergo) == value)[0][0]
                    MmI.append(index)
                else : 
                    allIndex = np.where(np.array(Ergo) == value)[0]
                    ind = 0
                    while ind < allIndex.shape[0]:
                        if allIndex[ind] != MmI[0] and allIndex[ind] != MmI[1] and Ergo[allIndex[ind] ] == value:
                            index = allIndex[ind]
                            ind = allIndex.shape[0]
                        ind+=1

                tps = time_post[index]

                L = allSCR[index]

                L = [beg_time,end_time,tps,value] + L

                L2 = allSCRDet[index]        
                L2 = [beg_time,end_time,tps,value] + L2

                M= np.array(L).reshape((1,len(L)))
                M2 = np.array(L2).reshape((1,len(L2)))
                if (L_Ergo_arm[l].shape[0] == 0):
                    L_Ergo_arm[l]= M
                    L_Ergo_arm_det[l] = M2
                else:
                    L_Ergo_arm[l] = np.r_[L_Ergo_arm[l], M]
                    L_Ergo_arm_det[l] = np.r_[L_Ergo_arm_det[l], M2]                
        print("File %s : treatement : %s done. "%(self.mot_file, 'Arm') )
        
        ## then, with Ergo_oth

        self.firstPost = True
        self.armSupported = True


        for keys in self.D_ind_time_oth:
            ## select Q
            posture_times = self.D_ind_time_oth[keys]
            beg_time = posture_times[0][0]
            end_time = posture_times[-1][1]

            beg_index = np.where(time == beg_time)[0][0]
            end_index = np.where(time == end_time)[0][0]

            q_post = Q[beg_index:end_index,:]
            time_post = time[beg_index:end_index]     

            if self.ergoEval == "RULA":

                Ergo, allSCR, allSCRDet = self.evaluateRULAPosture(time_post,q_post,y_calib,joint_names,keys,"Oth")   


            elif self.ergoEval == "REBA":

                Ergo, allSCR, allSCRDet = self.evaluateREBAPosture(time_post,q_post,y_calib,joint_names,keys,"Oth", self.staticAction_oth)   


            if self.firstPost:
                if self.ergoEval == "RULA":
                ## take median neck angle for 1st posture (calib)

                    #exit()
                    q_neck = q_post[:,10]

                    q_neck = q_neck[:,0]

                    # print(np.median(q_neck,axis = 0)[0,0])

                    self.neck_angle = np.median(q_neck,axis = 0)[0,0]
                    # print(self.neck_angle)
                    # exit()
                self.firstPost = False
                self.armSupported = oriArmSup


            MmI = []
            Lvalues = []
            MmIErgo = []
            
            print("File %s : treatement : %s ; percentage of treatment : %3.2f "%(self.mot_file, 'Oth', beg_index*100.0/Q.shape[0]) )

            ## TODO : print all value + Ergo values for all MmI
            for l in range(3):

                value = -1

                if l == 0:

                    value = np.max(Ergo)
                
                elif l == 1:
                    value = np.min(Ergo)                    
                
                elif l == 2:
                    value = int(np.median(Ergo))


                index = -1
                if (l < 2):
                    #allIndex = np.where(np.array(Ergo) == value)
                    # print(allIndex)
                    # print(value)
                    # exit()
                    index = np.where(np.array(Ergo) == value)[0][0]
                    MmI.append(index)
                    Lvalues.append(value)
                    MmIErgo.append(Ergo[index])
                else : 
                    allIndex = np.where(np.array(Ergo) == value)[0]
                    ind = 0
                    while ind < allIndex.shape[0]:
                        if allIndex[ind] != MmI[0] and allIndex[ind] != MmI[1] and Ergo[allIndex[ind] ] == value:
                            index = allIndex[ind]
                            ind = allIndex.shape[0]
                        ind+=1
                    MmI.append(index)
                    Lvalues.append(value)
                    MmIErgo.append(Ergo[index])
                    # print(MmI)
                    # print("val : ", Lvalues)
                    # print("val Ergo : ", MmIErgo)
                
                tps = time_post[index]

                L = allSCR[index]

                L = [beg_time,end_time,tps,value] + L

                L2 = allSCRDet[index]        
                L2 = [beg_time,end_time,tps,value] + L2

                M= np.array(L).reshape((1,len(L)))
                M2 = np.array(L2).reshape((1,len(L2)))
                if (L_Ergo_oth[l].shape[0] == 0):
                    L_Ergo_oth[l]= M
                    L_Ergo_oth_det[l] = M2
                else:
                    L_Ergo_oth[l] = np.r_[L_Ergo_oth[l], M]
                    L_Ergo_oth_det[l] = np.r_[L_Ergo_oth_det[l], M2]                
        print("File %s : treatement : %s done. "%(self.mot_file, 'Oth') )
        # print("For arm : ")
        # print(Ergo_arm)
        # print("For other : ")
        # print(Ergo_oth)

        # exit()

        return(L_Ergo_arm,L_Ergo_oth,L_Ergo_arm_det,L_Ergo_oth_det)



    def findMuscleScoreTime(self,D_ind_time):
        L_add_rep = []
        L_add_static = []
        for keys in D_ind_time:
            all_times = D_ind_time[keys]

            ## check if repetition >=4 in one minute
            i = 0
            while (i < len(all_times)-4):

                if all_times[i+4][1] - all_times[i][0] < 60.0:
                    j = i+4
                    while ( (j < len(all_times) ) and ( (all_times[j][1] - all_times[i][0]) < 60.0 )):
                        j+=1
                    ## save interval 
                    j = min(j,len(all_times)-1)

                    L_add_rep.append([all_times[i][0],all_times[j][1] ])
                    i = j
                else:
                    i+=4
            
            ## check if a posture is the same for more than one minute
            for k in range(len(all_times)):
                if (all_times[k][1] - all_times[k][0] > 60.0):
                    L_add_static.append([all_times[k][0],all_times[k][1] ])
        
        ## now, fuse the times
        L_add = []
            
        # i = 0
        # while i < len(L_add_rep):
        #     times_i = L_add_rep[i]
        #     j = 0
        #     while j < len(L_add_static):
        #         times_j = L_add_static[j]
        #         i_in_j = (times_i[0] > times_j[0] and  times_i[1] < times_j[1])
        #         j_in_i = (times_j[0] > times_i[0] and  times_j[1] < times_i[1])

        #         i_and_j_separed = (times_i[1] < times_j[0] or times_j[1] > times_i[0])

        #         if i_and_j_separed:
        #             j+=1

        #         elif i_in_j:
        #             ## L_add_rep[i] do not gives any infos : suppress it
        #             del(L_add_rep[i])
        #             j = len(L_add_static)

        #         elif j_in_i:
        #             ## L_add_static[j] do not gives any infos : suppress it
        #             del(L_add_static[j])

        #         else:
        #             ## times_i and times_j are not separed 
        #             ## but are not totally one in the other :
        #             ## fuse those intervals in times_i
        #             max_time = max(times_i[1], times_j[1])
        #             min_time = min(times_i[0], times_j[0])
        #             L_add_rep[i] = [min_time,max_time]
        #             del(L_add_static[j])
        #             j = 0

        L_add = L_add_rep+L_add_static
                                
        return(L_add)


    def findSamePostureIntervals(self,time,data, timePos = 1, diffPos = 2*np.pi/180, verbose = False):
        """!
        
        Idea : 
            * each time, check when same posture
            * if same posture (= same Q for a time > timePos) :
                - save time when same posture
                - when over, check with other saved time posture

        diffPos = [linear_diff (m), angular_diff (rad)]

        Output : 
            D = {
                Q : [ [time1], [time2], ...  ]

            }
        
        """

        indPos = 0
        D_ind_time = {}
        D_ind_pos = {}
        checkedPosture = data[0,:]
        # print(checkedPosture.shape)
        # exit()
        findPosture = False

        postureTime = [time[0],-1]
        for i in range(1,len(time)):
             
            #display = (postureTime[1] - postureTime[0]) > 0.2
            display = False

            if all_close(checkedPosture,data[i,:],diffPos,1,display ):
                ## same posture, save time
                postureTime[1] = time[i]
                if (postureTime[1] - postureTime[0] > timePos):
                    ## same posture for enough time, save it
                    findPosture = True
            else:
                if findPosture:
                    keys = list(D_ind_pos.keys())
                    foundPosture = False
                    i = 0
                    while not foundPosture and i < len(keys):
                        if all_close( D_ind_pos[keys[i]], checkedPosture,diffPos):
                            D_ind_time[keys[i]].append([postureTime[0], postureTime[1]])
                            foundPosture = True
                        else:
                            i+=1
                    if not foundPosture:
                        ## means that posture is new : add it
                        D_ind_pos[indPos] = checkedPosture
                        D_ind_time[indPos] = [ [postureTime[0], postureTime[1]]]
                        indPos += 1
                if verbose and display:
                    print("Found between : ", postureTime, " for : %3.4f time."%(postureTime[1]- postureTime[0]))
                
                checkedPosture = data[i,:]
                postureTime[0] = time[i]
                postureTime[1] = -1
                findPosture = False

        if findPosture:
            keys = list(D_ind_pos.keys())
            foundPosture = False
            i = 0
            while not foundPosture and i < len(keys):
                if all_close( D_ind_pos[keys[i]], checkedPosture,diffPos):
                    D_ind_time[keys[i]].append(postureTime)
                    foundPosture = True
                else:
                    i+=1
            if not foundPosture:
                ## means that posture is new : add it
                D_ind_pos[indPos] = checkedPosture
                D_ind_time[indPos] = [postureTime]
                indPos += 1
        
        return(D_ind_pos,D_ind_time)


    def getJointProps(self,L_name):
        for joint,name in zip(self.urdf_model.joints,self.urdf_model.names):
            nq = joint.nq
            idx_q = joint.idx_q
            self.D_name_qInd[name] = [nq,idx_q]
        for k in range(len(L_name)):
            self.D_name_ind[L_name[k]] = k

    def setQ(self,q_given,L_name):
        q=  self.lastQ
        for i in range(q_given.shape[1]):
            name = L_name[i]
            nq,idx_q = self.D_name_qInd[name]
            q[idx_q:idx_q+nq] = q_given[0,i]
        #print("q : ", q)
        se3.forwardKinematics(self.urdf_model,self.urdf_data,q)
        self.lastQ = q
        
    def getJointPose(self,name):
        urdf_id = self.urdf_model.getJointId(name)
        jp= self.urdf_data.oMi[urdf_id]
        M_urdf = np.eye(4,4)
        M_urdf[:3,3] = np.array(jp.translation)
        M_urdf[:3,:3] = jp.rotation
        return(M_urdf)
        




if __name__ == "__main__":

    urdf_filename = "/home/auctus/sensoring_ws/opensim_ws/src/human_control/description/full_upper_body_generalized/full_upper_body_generalized_aCol.urdf.xacro"

    # mot_path = "/home/auctus/sensoring_ws/opensim_ws/Documents/App_data/Session-2023-03-09/mocap_skin/IK/Ergo"

    # # security
    # mot_name = "Take 2023-03-09 04.31.27 PM_calib_frame_359_ik.mot"
    # mot_name = "Take 2023-03-09 04.34.05 PM_calib_frame_405_ik.mot"    


    # # direct
    # mot_name = "Take 2023-03-09 04.28.34 PM_calib_frame_336_ik.mot"
    # #mot_name = "Take 2023-03-09 04.29.20 PM_calib_frame_417_ik.mot"
    # # mot_name = "Take 2023-03-09 04.30.05 PM_calib_frame_465_ik.mot"    

    mot_path ="/home/auctus/sensoring_ws/opensim_ws/src/addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK"

    mot_path = [
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_1_1_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_1_2_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_2_1_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_2_2_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_2_3_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_3_1_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_3_2_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_4_1_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_4_2_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_5_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_6_1_ik.mot",
        "/home/auctus/sensoring_ws/opensim_ws/src/auto_trc_labeling/../addbiomechanicslocal/scripts/addbiomechanicslocal/data/BL/osim_results/IK/trial_6_2_ik.mot"

    ]

    #mot_filename = mot_path+mot_name



    ErgoEval(urdf_filename,mot_path,100, desactivateLegsComputing=True)
